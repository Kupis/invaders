﻿using System;
using System.Windows;

namespace Invaders.Model
{
    class Invader : Ship
    {
        public static readonly Size InvaderSize = new Size(15, 15);
        public InvaderType InvaderType { get; private set; }
        public int Score { get; private set; }

        public const int HorizontalInterval = 5;
        public const int VerticalInterval = 15;

        public Invader(Point location, InvaderType type, int score)
            : base(location, InvaderSize)
        {
            InvaderType = type;
            Score = score;
        }

        public override void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Right:
                    Location = new Point(Location.X + HorizontalInterval, Location.Y);
                    break;
                case Direction.Left:
                    Location = new Point(Location.X - HorizontalInterval, Location.Y);
                    break;
                default:
                    Location = new Point(Location.X, Location.Y + VerticalInterval);
                    break;
            }
        }

    }
}

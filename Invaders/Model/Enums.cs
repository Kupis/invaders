﻿namespace Invaders.Model
{
    enum Direction
    {
        Right = 0,
        Down = 1,
        Left = 2,
        Up = 3
    }

    enum InvaderType
    {
        Bug,
        Saucer,
        Satellite,
        Spaceship,
        Star
    }
}

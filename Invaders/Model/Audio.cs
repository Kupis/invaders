﻿using System;
using System.Media;
using System.Windows.Media;

namespace Invaders.Model
{

     class Audio
    {
        private static MediaPlayer backgroundMusic = new MediaPlayer();
        private static SoundPlayer enemyShot = new SoundPlayer(Properties.Resources._270344__littlerobotsoundfactory__shoot_00);
        private static SoundPlayer playerShot = new SoundPlayer(Properties.Resources._270343__littlerobotsoundfactory__shoot_01);
        private static SoundPlayer enemyKilled = new SoundPlayer(Properties.Resources._441497__mattix__retro_explosion_05);
        private static SoundPlayer playerKilled = new SoundPlayer(Properties.Resources._403297__mattix__retro_explosion_03);
        private static SoundPlayer defeatMusic = new SoundPlayer(Properties.Resources.Monplaisir___09___Defeat);

        static public void backgroundMusicStartLoop()
        {
            backgroundMusic.Open(new Uri("Desert of Lost Souls.mp3", UriKind.RelativeOrAbsolute));
            backgroundMusic.Play();
        }

        static public void backgroundMusicStop()
        {
            backgroundMusic.Stop();
        }

        static public void enemyShotPlay()
        {
            enemyShot.Play();
        }

        static public void playerShotPlay()
        {
            playerShot.Play();
        }

        static public void enemyKilledPlay()
        {
            enemyKilled.Play();
        }

        static public void playerKilledPlay()
        {
            playerKilled.Play();
        }

        static public void defeatMusicPlay()
        {
            defeatMusic.Play();
        }
    }
}

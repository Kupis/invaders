# Invaders

WPF .NET homage to one of the most popular icon in video game history!
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman

## Purpose

The purpose of this application is to test all knowledge from "Head First C#" book.

## Assumptions

### Visual

* Invaders' ships are animated with blocky, pixelated, retro 80s-style graphics,
* The whole playing area has a 4:3 aspect ratio just like an old arcade cabinet,
* Screen is completed with simulated scan lines to make it look authentic,
* The multicolored stars in the background twinkle on and off (don't affect gameplay at all),
* In the upper-righthand corner is located score,
* Player's spare ships are shown underneath the score,
* When the player dies, player's ship will flash for 2.5 seconds.

### Controls

* Movement:
  - Right: using the right key or key D,
  - Left: using the left key or key D,
* Shot: using the spacebar, key W or up key.

### Invaders

* Invaders attack in waves,
* Each wave is tight formation of 66 individual invaders (11 columns with 6 invaders),
* Invaders types:
  - The stars are worth 10 points,
  - The satellites are worth 20 points,
  - The saucers are worth 30 points,
  - The bugs are worth 40 points,
  - Those pesky alien invader spaceships are worth 50 points.
* Invaders behavior:
  - They start at the top of the screen and move left until they reach the edge. Then they drop down and start moving right,
  - When they reach the righthand boundary, they drop down and move left again,
  - Each wave can shot (1 + wave number) shots at once (for example wave #3 fires four shots),
* Invaders are dying from 1 shot.

### Player

* Player have 3 ships - the first ship is in play and the other two are kept in reserve,
* Player ships are destroyed by 1 shot,
* There can only be three player shots on the screen at once,
* Player moves only left and right.

## Sounds credits

* MATTIX from freesound.org :
	- 403297__mattix__retro-explosion-03
	- 441497__mattix__retro-explosion-05
* LittleRobotSoundFactory from freesound.org :
	- 270343__littlerobotsoundfactory__shoot-01
	- 270344__littlerobotsoundfactory__shoot-00
* Monplaisir from freemusicarchive.org
	- Monplaisir_-_09_-_Defeat
* Kevin MacLeod from incompetech.com
	- Desert of Lost Souls


## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.